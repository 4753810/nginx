FROM debian:9 as build 
ENV VER_NGINX_DEVEL_KIT=0.3.0
ENV VER_LUA_NGINX_MODULE=0.10.5
ENV VER_NGINX=1.10.0
ENV VER_LUAJIT=2.1.0-beta2
ENV NGINX_DEVEL_KIT ngx_devel_kit-${VER_NGINX_DEVEL_KIT}
ENV LUA_NGINX_MODULE lua-nginx-module-${VER_LUA_NGINX_MODULE}
ENV NGINX_ROOT=/etc/nginx
ENV WEB_DIR /var/www/html
ENV LUAJIT_LIB /usr/local/lib
ENV LUAJIT_INC /usr/local/include/luajit-2.1
ENV CONFIG "\
    --sbin-path=/usr/sbin/nginx \
	--modules-path=/usr/lib/nginx/modules \
	--conf-path=$NGINX_ROOT/nginx.conf \
	--error-log-path=/var/log/nginx/error.log \
	--http-log-path=/var/log/nginx/access.log \
    --prefix=$NGINX_ROOT \
    --with-ld-opt='-Wl,-rpath,$LUAJIT_LIB' \
    --add-module=/$NGINX_DEVEL_KIT \
    --add-module=/$LUA_NGINX_MODULE \
	"
RUN apt update -y && apt install -y gcc make wget curl libpcre3 libpcre3-dev openssl zlib1g-dev 
RUN wget http://nginx.org/download/nginx-${VER_NGINX}.tar.gz
RUN wget http://luajit.org/download/LuaJIT-${VER_LUAJIT}.tar.gz
RUN wget https://github.com/simpl/ngx_devel_kit/archive/v${VER_NGINX_DEVEL_KIT}.tar.gz -O ${NGINX_DEVEL_KIT}.tar.gz
RUN wget https://github.com/openresty/lua-nginx-module/archive/v${VER_LUA_NGINX_MODULE}.tar.gz -O ${LUA_NGINX_MODULE}.tar.gz
RUN tar -xzvf nginx-${VER_NGINX}.tar.gz && rm nginx-${VER_NGINX}.tar.gz
RUN tar -xzvf LuaJIT-${VER_LUAJIT}.tar.gz && rm LuaJIT-${VER_LUAJIT}.tar.gz
RUN tar -xzvf ${NGINX_DEVEL_KIT}.tar.gz && rm ${NGINX_DEVEL_KIT}.tar.gz
RUN tar -xzvf ${LUA_NGINX_MODULE}.tar.gz && rm ${LUA_NGINX_MODULE}.tar.gz
WORKDIR /LuaJIT-${VER_LUAJIT}
RUN make
RUN make install
WORKDIR /nginx-${VER_NGINX}
RUN ./configure $CONFIG
RUN make
RUN make install

FROM debian:9 
ENV NGINX_ROOT=/etc/nginx
ENV WEB_DIR /var/www/html
WORKDIR ${WEB_DIR}
COPY --from=build /usr/sbin/nginx /usr/sbin/nginx
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/include/luajit-2.1 /usr/local/include/luajit-2.1
COPY --from=build /etc/nginx /etc/nginx
RUN mkdir -p /var/log/nginx ; mkdir -p /var/cache/nginx ; \ 
    touch /var/log/nginx/access.log ; \
    touch /var/log/nginx/error.log ; \
    ln -sf /dev/stdout /var/log/nginx/access.log; \
    ln -sf /dev/stderr /var/log/nginx/error.log; 
ADD ./nginx.conf /${NGINX_ROOT}/nginx.conf
ADD ./nginx_base.conf /${NGINX_ROOT}/conf.d/default.conf
EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]










